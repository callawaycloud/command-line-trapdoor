on run argv
    tell application "Trapdoor"
        set x to every credential where username contains (item 1 of argv)
        set y to first item of x
        y login
    end tell
end run