#!/usr/bin/env node

//libaries
var exec = require('child_process').exec;
var argv = require('minimist')(process.argv.slice(2));
var path = require('path');

var un = argv._[0];
console.log('Opening ' + un + ' via trapdoor');
exec('osascript "' + path.resolve(__dirname, 'sfdcopen.scpt') + '" ' + un, function(error, stdout, stderr) {
    if (error) {
        console.log('something went wrong, sorry', error);
    }
});
